/**
 * Copyright (c) Pearson, Inc.
 * All rights reserved.
 *
 *
 * @module MediaAssets
 * @file assetsContainer -  The container fetches media assets and 
 then renders the dynamic media data in corresponding sub-component.
 * @author TDC
 */

import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {fetchingAssets, selectedRecord} from '../action/assets';
import {getSearchProductItems,saveSearchValues} from '../action/SearchLibraryAction';
import assetsGenerator from '../components/browse/assetsGenerator';
import serviceUrls from '../constants/service';
import {getCurrentValues} from '../utils/util';
import {DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS} from '../constants/paginationConstants';

/**@function getSelectedValues -
 * This method is used to get the selected values by user.
 * @param {object} dataArray - Array containing values selected by user
 * @returns {string} - If array length is greater than 0 , it will return last element of that array
 * @returns {object} array - else it will return empty array object
*/
const getSelectedValues = (dataArray) => {
  if (dataArray.size > 1) {
    let latestItem = dataArray.size-1;
    return dataArray.get(latestItem);
  }

  return [];
}


/**@function mapStateToProps -
 * Connects a React component to a Redux store.
 * Whenenver redux store gets updated, this method will get called.
 * This method transform the current application state into the
 * props you want to pass to a presentational component
 * @param {object} state
 * @returns {object} Object
*/
const mapStateToProps = (state) => {
  let data = getCurrentValues(state.assets);
  let selectedRecord = getSelectedValues(state.quad);
  let temp = null;

  if (data.length !== 0) {
    temp = JSON.parse(JSON.stringify(data.items));
  }

  return {
   assetsData: temp,
   pageDetails: Array.isArray(data)? {}: data,
   selectedRecord: Array.isArray(selectedRecord)? {}: selectedRecord,
   isSearchLibrary: data.showSaveSearch
  }
}
/**@function mapDispatchToProps
 * Connects a React component to a Redux store.
 * This method receives the dispatch() method and returns callback props that needs to be
 * injected into the presentational component
 * @param {function} dispatch
 * @returns {object} callback props
*/
const mapDispatchToProps = (dispatch) => {
  return {
  	 handlePageChange: function onSelect(page, event, sortValue) {
      event.preventDefault();
      let nodeRef;
      let filterUrlsForSearch = {
        0:serviceUrls.searchBasedAll,
        1:serviceUrls.searchBasedImage,
        2:serviceUrls.searchBasedVideo,
        3:serviceUrls.searchBasedAudio
      };
      let viewName;
      if(document.querySelector('.dropdown-display span i').className==='fa fa-list'){
        viewName = 'list-view';
      }else{
        viewName = 'grid-view';
      }
      let maxItems = parseInt(document.querySelector('#itemPerPageSelectBox').value);
      nodeRef = document.querySelector('.filter-container .tree-node-selected');
      //nodeRef = document.querySelector('.filter-container .pe_filter_enabled');
      if (nodeRef) {
        let id = nodeRef.id;
        dispatch(fetchingAssets(id, page,maxItems,'',sortValue,viewName));
      }else{
        let id = serviceUrls.defaultNodeRef;
        dispatch(fetchingAssets(id, page,maxItems,'',sortValue,viewName));
      }
      },

      onChange:function (event){
        event.preventDefault();
        let viewName;
      if(document.querySelector('.dropdown-display span i').className==='fa fa-list'){
        viewName = 'list-view';
      }else{
        viewName = 'grid-view';
      }
        let nodeRef;
        nodeRef = document.querySelector('.filter-container .tree-node-selected');
        //nodeRef = document.querySelector('.filter-container .pe_filter_enabled');
        if (nodeRef) {
        let id = nodeRef.id;
        dispatch(fetchingAssets(id, DEFAULT_PAGE_NO,parseInt(event.target.value),'','',viewName));
        }else{
         let id = serviceUrls.defaultNodeRef;
        dispatch(fetchingAssets(id, DEFAULT_PAGE_NO,parseInt(event.target.value),'','',viewName));
      }
      },

      setSelectedItem: function (record) {
        dispatch(selectedRecord(record));
      },

      saveSearch:function (event){
        event.preventDefault();
        if(document.querySelector('.react-autosuggest__input').value){
          let SearchValue = document.querySelector('.react-autosuggest__input').value;
          dispatch(saveSearchValues(SearchValue));
        }
      },

      onSort: function (sortValue, viewName){ 
        let nodeRef;
        let maxItems = parseInt(document.querySelector('#itemPerPageSelectBox').value);
        nodeRef = document.querySelector('.filter-container .tree-node-selected');
        //nodeRef = document.querySelector('.filter-container .pe_filter_enabled');
        if (nodeRef) {
        let id = nodeRef.id;
        dispatch(fetchingAssets(id, DEFAULT_PAGE_NO,maxItems,'', sortValue,viewName));
        }else{
         let id = serviceUrls.defaultNodeRef;
        dispatch(fetchingAssets(id, DEFAULT_PAGE_NO,maxItems,'',sortValue,viewName));
      }
  },

    changeView:function (viewName){
        let maxItems;
        if(viewName === 'list-view'){
          maxItems = 25;
        }else{
          maxItems = 9;
        }
        let nodeRef;
        nodeRef = document.querySelector('.filter-container .tree-node-selected');
        //nodeRef = document.querySelector('.filter-container .pe_filter_enabled');
        if (nodeRef) {
        let id = nodeRef.id;
        dispatch(fetchingAssets(id, DEFAULT_PAGE_NO,maxItems,'','',viewName));
        }else{
         let id = serviceUrls.defaultNodeRef;
        //let maxItems = parseInt(document.querySelector('#itemPerPageSelectBox').value);
        dispatch(fetchingAssets(id, DEFAULT_PAGE_NO,maxItems,'','',viewName));
      }
      }
}
}

const assetsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(assetsGenerator)

export default assetsContainer;
