/**
 * Copyright (c) Pearson, Inc.
 * All rights reserved.
 *
 *
 * @module MediaAssets
 * @file SearchLibraryContainer - This container does data fetching
  and then renders its corresponding sub-component.
 * @author TDC
 */
import {connect} from 'react-redux';
import SearchLibrary from '../components/SearchLibrary';
import {getSearchProductItems,
  getDifficultyLevels,
  saveSearchValues,
  deleteSavedSearch,
  runSearch } from '../action/SearchLibraryAction';
import { Link, browserHistory, hashHistory } from 'react-router'
import {DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS} from '../constants/paginationConstants';
import _ from 'lodash';
import {getCurrentValues} from '../utils/util';

const localforage = require('localforage');

/**@function getSelectedValues -
 * This method is used to get the selected values by user.
 * @param {object} dataArray - Array containing values selected by user
 * @returns {string} - If array length is greater than 0 , it will return last element of that array
 * @returns {object} array - else it will return empty array object
*/
const getSelectedValues = (dataArray) => {
  if (dataArray.length > 0) {
    return dataArray[dataArray.length-1];
  }

  return [];
}

/**@function mapStateToProps -
 * Connects a React component to a Redux store.
 * Whenenver redux store gets updated, this method will get called.
 * This method transform the current application state into the
 * props you want to pass to a presentational component
 * @param {object} state
 * @returns {object} Object
*/
const mapStateToProps = (state) => {
    let productName = '';
    let data = getSelectedValues(state.searchLibraryReducer);
    let selectedData = getCurrentValues(state.quad);

    if(state.autoComplete.length > 0){
        productName = state.autoComplete[state.autoComplete.length-1].text;
     }
     return {
     enableSearch: state.searchLibraryReducer.enableSearch,
    enableDelete: state.searchLibraryReducer.enableDelete,
    isSavedSearch: data.isSavedSearch,
    record: Array.isArray(selectedData)? {}: selectedData,
    showAssets:false,
    'SearchValue':productName,
    'initialValues': {
            productName:productName
          }
        }
}

/**@function mapDispatchToProps
 * Connects a React component to a Redux store.
 * This method receives the dispatch() method and returns callback props that needs to be
 * injected into the presentational component
 * @param {function} dispatch
 * @returns {object} callback props
*/
const mapDispatchToProps = (dispatch) => {
  return {
  		componentWillMount() {
  			//getSavedSearchValues();
  		},
      componentWillReceiveProps(){

      },
      getSearchProduct(value,dispatch){

        dispatch(getDifficultyLevels());
      //dispatch(getSearchProductItems(value.productName,1));
      	let searchValue = value;

        localforage.getItem('last_three_search').then(function (lastvalue){
      //  console.log(lastvalue);
      //  console.log(searchValue);

        if(searchValue.productName !== undefined && searchValue.productName !== ''){
        	let chkVal = _.find(lastvalue, { 'term': searchValue.productName});
            if(chkVal === undefined){
            	let sval = {term:searchValue.productName};
	            if(lastvalue.length >= 3){
				   lastvalue.pop(lastvalue.unshift(sval));
	            }else{
	               lastvalue.unshift(sval);
	            }
            }

        }

			localforage.setItem('last_three_search', lastvalue, function (err, val) {
				console.log(val);
          let searchString = document.querySelector('#addAnAssets .react-autosuggest__input').value;
		       dispatch(getSearchProductItems(searchString,DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS));
		        document.querySelectorAll('#displayContainerDiv')[0].style.display = 'block';

				let selectedTab = document.querySelector('#filterAssets .ReactTabs__Tab--selected').textContent;
					if(selectedTab === 'Saved Search'){
						document.querySelectorAll('#filterAssets .ReactTabs__Tab')[0].click();
					}

			});

      })
   },

    sendToQuad: function (record) {
      let temp = JSON.stringify(this.props.record);
      console.log(temp);
      // alert(temp);
      hashHistory.push('/ReviewAsset');
    }

  }
	}

const SearchLibraryContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchLibrary)

export default SearchLibraryContainer;
