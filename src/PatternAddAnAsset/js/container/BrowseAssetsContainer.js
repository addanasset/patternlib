/**
 * Copyright (c) Pearson, Inc.
 * All rights reserved.
 *
 *
 * @module MediaAssets
 * @file BrowseAssetsContainer - A container does data fetching and then renders its corresponding sub-component.
 * @author TDC
 */

import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {fetchingAssets} from '../action/assets';
import BrowseAssets from '../components/BrowseAssets';
import {getCurrentValues} from '../utils/util';
import { Link, browserHistory, hashHistory } from 'react-router'
/**@function mapStateToProps -
 * Connects a React component to a Redux store.
 * Whenenver redux store gets updated, this method will get called.
 * This method transform the current application state into the
 * props you want to pass to a presentational component
 * @param {object} state
 * @returns {object} Object
*/
const mapStateToProps = (state) => {
 let data = getCurrentValues(state.quad)
  return {
    record: Array.isArray(data)? {}: data
  };
}

/**@function mapDispatchToProps
 * Connects a React component to a Redux store.
 * This method receives the dispatch() method and returns callback props that needs to be
 * injected into the presentational component
 * @param {function} dispatch
 * @returns {object} callback props
*/
const mapDispatchToProps = (dispatch) => {
  return {
    sendToQuad: function (record) {
      let temp = JSON.stringify(this.props.record);
      console.log(temp);
      // alert(temp);
       hashHistory.push('/ReviewAsset');
    }
  };
}

const BrowseAssetsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(BrowseAssets)

export default BrowseAssetsContainer;
