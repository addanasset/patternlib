/**
 * Copyright (c) Pearson, Inc.
 * All rights reserved.
 *
 * This layer used for connecting to different external servers.
 *
 * @module MediaAssets
 * @file fileUploadApi - This layer is used for uploading a file to server
 * @author TDC
 *
*/
const request = require('superagent');
const  Promise = require('bluebird');
import serviceUrls from '../constants/service';


import { hashHistory } from 'react-router'

export default {

   check_ticket_valid(token){
   	return Promise.promisifyAll(
	      request
	     .get(serviceUrls.loginUrl+'/ticket/'+token)
	     .auth(serviceUrls.alfrescoUname, serviceUrls.alfrescoPwd)
	     )

   },
   get_token(){
		  return Promise.promisifyAll(
		  	request
	     .post(serviceUrls.loginUrl)
	     .send({'username' : serviceUrls.alfrescoUname,'password' : serviceUrls.alfrescoPwd})
		  )
   },

   get_Data() {

    return Promise.promisifyAll(
	      request
	     .get(serviceUrls.loginUrl)
	     .query({ u: serviceUrls.alfrescoUname })
	     .query({ pw: serviceUrls.alfrescoPwd })
	     //.auth('admin', 'admin')
	     .on('progress', function (e){
          //console.log(e.direction,'is done',e.percent,'%');
         })
     )
  },

   post_Data(formData,values,token,currFolderId,dispatch, getState) {
   	//let defaultNodeRef = '1a6b62f6-0efb-4448-b405-6c014350191e'; //Temporary fix
   	let fileName = formData.get('filedata').name;
    let title = values.name;    
	let Aflerco_Upload_Url =serviceUrls.fileUploadUrl+'-default-/public/cmis/versions/1.1/browser/root?' +
	'objectId=workspace://SpacesStore/' + currFolderId +
	'&cmisaction=createDocument&overwriteFlag=true&propertyId[0]=cmis:name&propertyValue[0]=' + fileName +
	'&propertyId[1]=cmis:objectTypeId&propertyValue[1]=cmis:document'+
	'&propertyId[2]=cmis:secondaryObjectTypeIds&propertyValue[2]=P:cm:titled'+
	'&propertyId[3]=cm:title&propertyValue[3]='+ title +'&alf_ticket=' + token;

	let apiData = {
	 rows :  [
	          {
	            Name: title,
	            Size: 0+'kb',
	            Progress: 0,
	            status:'Uploading'
	          }
	      ]
	    }
	    dispatch({
			type : 'JOB_STATUS',
			data : apiData
		})
	hashHistory.push('/CheckJobStatus');
    return Promise.promisifyAll(
           request
           .post(Aflerco_Upload_Url)
           .send(formData)
           .on('progress', function (e){

			let state =  getState();
			let currstate = state.CheckJobStatusReducers[state.CheckJobStatusReducers.length-1];
			  let progressData = {
		  	       rows :  [
		  	        {
		  			Name: title,
		            Size: Math.round(e.loaded/1024)+'kb',
		            Progress: Math.round(e.percent),
		            status:'Uploading'
		           }
		          ]
			  }

              if(e.target.readyState === undefined && e.target.status === undefined){
					dispatch({
					type : 'JOB_STATUS_UPDATE',
					data : progressData
					})

              }
           })
    	)
  },


  file_Upload(formData,values,token,folderID,dispatch, getState){
    let _folderID = folderID;

    if(_folderID ==''){
		_folderID = serviceUrls.defaultNodeRef;
    }
    console.info('_folderID:' + _folderID);

  	formData.append('containerid', '');
	formData.append('siteid', '');
	formData.append('uploaddirectory', '');
	formData.append('overwrite', '\"false\"');
	formData.append('destination', 'workspace://SpacesStore/'+_folderID);
  	const file_Upload_Url = serviceUrls.fileUploadUrl+'/upload';

	let apiData = {
	 rows :  [
	          {
	            Name: formData.get('filedata').name,
	            Size: 0+'kb',
	            Progress: 0,
	            status:'Uploading'
	          }
	      ]
	    }

		dispatch({
			type : 'JOB_STATUS',
			data : apiData
		})



    hashHistory.push('/CheckJobStatus');
  	return Promise.promisifyAll(
           request
           .post(file_Upload_Url)
           .auth(serviceUrls.alfrescoUname,serviceUrls.alfrescoPwd)
           .send(formData)
           .on('progress', function (e){
			let state =  getState();
			let currstate = state.CheckJobStatusReducers[state.CheckJobStatusReducers.length-1];

			  let progressData = {
		  	       rows :  [
		  	        {
		  			Name: formData.get('filedata').name,
		            Size: Math.round(e.loaded/1024)+'kb',
		            Progress: Math.round(e.percent),
		            status:'Uploading'
		           }
		          ]
			  }

              if(e.target.readyState === undefined && e.target.status === undefined){
					dispatch({
					type : 'JOB_STATUS_UPDATE',
					data : progressData
					})
              }

              if(e.target.status === 200){
              	/*console.log(JSON.parse(e.target.responseText));
			    let result = JSON.parse(e.target.responseText);

			    let completeData = {
					  	       rows :  [
					  	        {
					  			Name: result.fileName,
					            Size: currstate.rows[0].Size,
					            Progress: 100,
					            status:'Success'
					           }
					          ]
						  }

              	dispatch({
					type : 'JOB_STATUS_UPDATE',
					data : completeData
					})
				*/
              }

			  //console.log(progressData);
			  //console.log(currstate);
           })
    	)

  }
}
