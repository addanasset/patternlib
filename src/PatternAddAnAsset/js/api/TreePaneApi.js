/**
 * Copyright (c) Pearson, Inc.
 * All rights reserved.
 * @class TreePaneApi
 * this will be used to call the respective service URL to get the Children
 * for Folder pane
 * @author Udhayakumar Gururaj
 **/
const request = require('superagent');
const  Promise = require('bluebird');
import serviceUrls from '../constants/service';

export default {

/**
* @function getRootChildren method is used for get the children from
* Alfreco service 
* @param {string} token
*/
getRootChildren(token) {
    let url = serviceUrls.folderData+'&alf_ticket='+token;
    console.log(url);

    let subfolderqry = 'select * from  cpwc:syncMD where cmis:baseTypeId = \'cmis:folder\' and contains(\'PATH:\"/app:company_home/st:sites/*"\')';
    console.log(subfolderqry);
    /*return Promise.promisifyAll(
          request.get(url)

        )*/

return Promise.promisifyAll(
	          request.get(serviceUrls.subfolderDataQuery)
	          .query({ cmisselector: 'query' })
	          .query({ q: subfolderqry })
	          .query({ alf_ticket: token })
	        )
  },

 getSubFolders(token,folderName,child) {
    let url = serviceUrls.folderData+'&alf_ticket='+token;
    let childurl = serviceUrls.subfolderData + child.path +'?&cmisselector=children&alf_ticket='+token;
    
    console.log(childurl);
    console.log(child.path);

    let nodeRef = child.nodeRef.split('/')[3]

     
    /*return Promise.promisifyAll(
          request.get(serviceUrls.subfolderData + child.path)
          .query({ cmisselector: 'children' })
          .query({ alf_ticket: token })
        )*/
    let subfolderqry =  'select * from  cpwc:syncMD where cmis:baseTypeId = \'cmis:folder\' and IN_FOLDER(\''+nodeRef+'\')'; 
	console.log(subfolderqry);

	return Promise.promisifyAll(
	          request.get(serviceUrls.subfolderDataQuery)
	          .query({ cmisselector: 'query' })
	          .query({ q: subfolderqry })
	          .query({ alf_ticket: token })
	        )
  }
}
