/**
 * Copyright (c) Pearson, Inc.
 * All rights reserved.
 *
 * This layer used for connecting to different external servers.
 *
 * @module MediaAssets
 * @file searchLibraryApi - This layer is used for retrieving assets data and for
   saving new saved search value.
  from local forage.
 * @author TDC
 *
*/
const request = require('superagent');
const  Promise = require('bluebird');
const localforage = require('localforage');
import serviceUrls from '../constants/service';
import {format} from '../utils/stringManipulation';
import MetaDataService from '../../../common/util/metadataService';

let filterUrl = serviceUrls.searchBasedAll;

/** @function autoComplete_Data -
 * request.get service call is used to get the autocomplete data from server.
 * @returns {function}
 * This function returns promise.
*/
function autoComplete_Data(text) {
 	const bufferGet = {
    action: 'Search',
    req: 'AddAnAssets',
    data: {
      searchterms: 'https://schema.pearson.com/ns/content/metadataType=AssetMetadata&http://schema.org/name='+text
    }
  }

    bufferGet.libConfig = window.tdc.libConfig;
    return MetaDataService.send(bufferGet);
  }
  /** @function getAssertsData -
 * request.get service call is used to get the assets data.
 * @returns {function}
 * This function returns promise.
*/
export default {
      autoComplete_Data: autoComplete_Data,
       getAssertsData(text, filter,index,limit, sortOption='cm:created|false') {
          if (filter !== '' && filter !== undefined) {
              filterUrl = filter;
          }

          const bufferGet = {
            action: 'Search',
            req: 'AddAnAssets',
            data: {
              searchterms: 'https://schema.pearson.com/ns/content/metadataType=AssetMetadata&http://schema.org/name='+text
            }
          }

          bufferGet.libConfig = window.tdc.libConfig;
          return MetaDataService.send(bufferGet);

        console.log('autocomplete data final');

/*    localforage.setItem('key11',null);
      localforage.setItem('savedSearch', []).then(function () {
       let a =  localforage.getItem('savedSearch');
       console.log('sdsdd'+a);
       localforage.getItem('savedSearch', function (err, readValue) {
            console.log('Read value is ', readValue);
            let savedSearch = readValue;
            if(Array.isArray(savedSearch)){
              savedSearch.push({'searchTerm':'polynomial', 'filter':'Difficult'});
              localforage.setItem('savedSearch',savedSearch);
            }
          });
    })*/
  },
  /** @function saveSearchValue -
 * localforage.getItem is used to existing saved search data.
    This function adds the new saved search value.
  */
  saveSearchValue(text) {
      return localforage.getItem('savedSearch', function (err, readValue) {
            let savedSearch = readValue;
            if(readValue === null || readValue.length===0){
              savedSearch =[];
            }
            if(Array.isArray(savedSearch)){
              if(savedSearch.length){
              //let i = savedSearch.length + 1;
              }
              let AlreadyExists =false;
              for(let j=0;j<savedSearch.length;j++){
                if(savedSearch[j].searchterm===text){
                  AlreadyExists = true;
                }
              }
              if(AlreadyExists){
                alert('Value already exists in Saved Search');
              }else{
              let randomId = Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
              savedSearch.unshift({'searchterm': text,'id':randomId, 'filter':'Difficult','isChecked':false});
              localforage.setItem('savedSearch',savedSearch);
               alert('Search value saved successfully');
            }
            }
          });

/*       autoComplete_Data() {
    return Promise.promisifyAll(
        request
       .get('http://localhost:3000/autoCompleteDataSugg')
      )
  }*/
  },

    getSearchValue(text) {
       localforage.getItem('savedSearch', function (err, readValue) {
            console.log('Read value is ', readValue);
          });

  },

  difficultyLevelData(){
    const bufferGet = {
    action: 'Taxonomies',
    req: 'difficultyLevels',
    data: {
      taxonomies: 'difficultylevel'
      }
    }

    bufferGet.libConfig = window.tdc.libConfig;
    return MetaDataService.send(bufferGet);

  }
}
