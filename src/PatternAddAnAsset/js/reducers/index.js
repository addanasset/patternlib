/**
 * Copyright (c) Pearson, Inc.
 * All rights reserved.
 *
 * @module MediaAssets
 * @file index - Combined Reducers. -
 * Turns an object whose values are different reducer functions, into a single
 * reducer function. It will call every child reducer, and gather their results
 * into a single state object, whose keys correspond to the keys of the passed
 * reducer functions.
 * One handy way to obtain
 * It is to use ES6 `import * syntax. The reducers may never return
 * undefined for any action. Instead, they should return their initial state
 * if the state passed to them was undefined, and the current state for any
 * unrecognized action.
 * @author TDC
 *
*/

import { combineReducers } from 'redux'
import fileUpload from './fileUpload'
import {reducer as formReducer} from 'redux-form';
import CheckJobStatusReducers from './CheckJobStatusReducers';
import assets from './assets';
import searchAssets from './searchAssets';
import quad from './quad';
import { routerReducer } from 'react-router-redux'
import TreePaneReducers from './TreePaneReducer';
import autoCompleteReducer from './autoCompleteReducer';
import searchLibraryReducer from './searchLibraryReducer';
import savedSearchReducers from './savedSearchReducers';
import SingleFileFolderReducer from './SingleFileFolderReducer';
import difficultyLevelReducer from './difficultyLevelReducer';
import ReviewAssetReducers from '../../../PatternReviewAsset/js/reducers/ReviewAssetReducer';

const rootMetaData = combineReducers({
	form: formReducer,
	routing: routerReducer,
	CheckJobStatusReducers,
    assets,
    searchAssets,
    quad,
	fileUpload,
	TreePaneReducers,
	SingleFileFolderReducer,
	autoComplete:autoCompleteReducer,
	searchLibraryReducer,
	savedSearchReducers,
	difficultyLevelReducer,
	ReviewAssetReducers
});

export default rootMetaData
