/**
 * Copyright (c) Pearson, Inc.
 * All rights reserved.
 *
 *
 * @module MediaAssets
 * @file autoCompleteReducer - This autoComplete reducer is a function
   that takes two parameters (state and action)and returns updated state.
   It is mainy used to get saved search values from server.
 * @author TDC
 *
*/

import {AUTO_COMPLETE} from '../constants/searchLibraryConstants'
let initilizeData = [{

  }]

/**@function autoCompleteReducer
 * This autoComplete reducer contains a action called AUTO_COMPLETE
 * Whenever dispatch call is made with action as an argument, reducer will catch this call and return updated state.
 * If state is undefined or null it will return initial state else it will merge current state with initial state
 * @param {object} state - state object of component.
 * @param {object} action - action object of component.
 * @returns {object} state-
 * updated state of component.
*/
const autoCompleteReducer = (state = initilizeData, action) => {
  switch (action.type) {
    case AUTO_COMPLETE:
      return[
        ...state, {
            data:action.data.results,
            text: action.text,
            savedSearch:action.savedSearch,
            lastThreeSearch:action.lastThreeSearch
        }
      ]
    default:
      return state
  }
}

module.exports= autoCompleteReducer;
