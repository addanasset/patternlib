import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import Pagination from './Pagination';
import {DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS} from '../../constants/paginationConstants';
//require('bootstrap/less/bootstrap.less');

class Paginate extends Component {
  constructor(props) {
    super();
    this.state = {
      activePage: 1
    };
    //this.handlePageChange = ::this._handlePageChange;
  }

  render() {
    const pageDetails = this.props.pageDetails
    let pageLimit = DEFAULT_MAX_RESULTS;
    if(this.props.pageDetails.pageLimit){
      pageLimit = this.props.pageDetails.pageLimit;
    }
    let libData;
      if(this.props.pageDetails.SearchValue){
      libData = ' for '+this.props.pageDetails.SearchValue;
    }else{
      let libData = ' in library : The Humanities: Culture, Continuity and Change, Volume II';
    }
    return (
    <div className='pagingdiv'>
      <div className='row'>
        <div className='col-md-8 pageDetail'>
        <span>Showing {pageDetails.index+1}-{pageDetails.index+pageDetails.totalRecords} of {pageDetails.numberFound} {libData}</span>
      </div>
         <div className={'col-md-4'}>
          <Pagination 
          activePage={pageDetails.pageNo} 
          itemsCountPerPage={pageLimit} 
          totalItemsCount={pageDetails.numberFound} 
          pageRangeDisplayed={5}
          noDataPageNo={0}
          onChange={this.props.handlePageChange}/>
        </div>
      </div>
    </div>
    );
  }
}

Paginate.propTypes = {
  pageDetails: PropTypes.object,
  handlePageChange:PropTypes.func
}

module.exports = Paginate;
