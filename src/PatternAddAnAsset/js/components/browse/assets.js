import React, { Component, PropTypes } from 'react';
import Radio from '../../../../common/components/Radio';
import PE_tooltip from '../../../../common/components/pe-tooltip';
import {injectIntl} from 'react-intl';
import {getModifiedOn} from '../../../../common/components/browseAssetUtil';
import ReactDOM from 'react-dom';


class assets extends Component {

  assetSelectedEvent(me, isTrue=true) {
    if (isTrue) {
      this.customFn(this.props.record);
    }
    else {
      this.customFn({});
    }
  }

  changeRadioButton(event) {
    if (event.target.type !== 'radio') {
      let radioComponent = ReactDOM.findDOMNode(this.refs.radioComp);

      radioComponent.checked = !radioComponent.checked;
      this.refs.radioComp.assetSelectedEvent('', radioComponent.checked);
    }
  }

  render() {
    const {formatDate} = this.props.intl;

     let item = this.props.productTemp,
        selectedRecord = this.props.selectedRecord,
        checked = false, fileSize, fileType = 'KB',
        setSelectedItem = this.props.setSelectedItem;

        fileSize = parseFloat(item.size/1024).toFixed(2);
        if (fileSize >= 1024) {
          fileSize = parseFloat(fileSize/1024).toFixed(2);
          fileType = 'MB'
        }
       if(item.mimetype.includes('image')===true){
        item.IconClass = 'fa-image';
       }
       else if(item.mimetype.includes('video')===true){
         item.IconClass = 'fa-video-camera';
       }
       else if(item.mimetype.includes('audio')===true){
         item.IconClass = 'fa-volume-down';
       }
       else if(item.mimetype.includes('')===true){
         item.IconClass = 'fa-file';
       }
       else{
        item.IconClass = 'fa-file';

       }

       if (selectedRecord && selectedRecord.nodeRef === item.nodeRef) {
          checked = true;
       }
       let fileName;
       if(item.title){
        fileName = item.title;
       }else{
       let splitName = item.name.split('.');
       fileName = splitName[0];
      }

      let modify = item.modifiedBy !== undefined ? 'Uploaded by: '+item.modifiedBy : '';
      let fileInfo = item.size !== undefined ? ' File size: '+ fileSize + fileType : '';
       let pageRender;
       let radioBtn = <Radio name='assetsCheckbox' ref='radioComp' record={item} checked= {checked} customFn = {setSelectedItem} parent = {this.assetSelectedEvent}/>
       let imgTag = <img src={item.url} className='card-img' alt='product image' />
       let self = this;
       if(this.props.pageView === 'grid-view'){
              pageRender = function ()
              { return (<div onClick={self.changeRadioButton.bind(self)} key={item.nodeRef}
                className='card-item1 search-result-box'>
              {radioBtn}
              <div className='thumbnail card-body'>
                {imgTag}
              </div>
              <footer className=''>
                <PE_tooltip position='right' content={fileName}>
                <label><a className='ellipsis_inline title'><strong>{fileName}</strong></a></label>
                </PE_tooltip>
                <div className='footer-icon'>
                    <i className= {'browse-tooltip fa ' + item.IconClass}></i>
                      <PE_tooltip position='right'
                      content={ modify +' Date uploaded: '
                      +formatDate(getModifiedOn(item.modifiedOn))+ fileInfo}>
                        <i className='fa fa-info-circle'></i>
                      </PE_tooltip>
                </div>
              </footer>
              </div>)
            }
         }else{

              pageRender = function (){ return (<div
                onClick={self.changeRadioButton.bind(self)} className='pe-jobstatus-page row'>
             <div className='col-md-3 list-view'>
                <span className='col-md-2 radio-box'>{radioBtn}</span>
                <span className='col-md-2 resource-img'>{imgTag}</span>
                <PE_tooltip position='right' content={fileName}>
                  <span className='col-md-8 fileName'>{fileName}</span>
                </PE_tooltip>
              </div>
              <div className='col-md-3 resource-type'>{item.mimetype}</div>
              <div className='col-md-3 added-by'>{item.modifiedBy}</div>
              <div className='col-md-3 date-modified'>{getModifiedOn(item.modifiedOn)}</div>
              </div>)
            }
         }

     return (
          <div>
            {pageRender()}
          </div>
    );
  }
}

assets.propTypes = {
      intl: PropTypes.object,
      assetSelectedEvent: PropTypes.func,
      record: PropTypes.object,
      productTemp: PropTypes.object,
      setSelectedItem: PropTypes.func,
      selectedRecord: PropTypes.object,
      pageView: PropTypes.string
  }
export default injectIntl(assets);
