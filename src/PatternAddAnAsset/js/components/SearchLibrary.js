/**
 * Copyright (c) Pearson, Inc.
 * All rights reserved.
 *
 * It is a Items display per page component.
 * This component operates as a "Controller-View".
 *
 * @module MediaAssets
 * @file searchLibrary
 * @author TDC
 *
*/
import React, { Component, PropTypes } from 'react';
import { Link, browserHistory, hashHistory } from 'react-router';
import {reduxForm} from 'redux-form';
import AddAnAsset from './AddAnAsset'
import Label from'../../../common/components/Label';
//import AutoCompleteContainer from '../container/autoCompleteContainer';
import SearchCompleteContainer from '../container/SearchCompleteContainer';
import SearchAssetFiltersContainer from '../container/SearchAssetFiltersContainer';
import SearchFilterContainer from '../container/SearchFilterContainer';


class SearchLibrary extends Component {
/**
  * @constructor defines states of the search library Component
  */
  constructor(props) {
    super(props);
    this.state = {
       showAssert: true,
       isSavedSearch: props.isSavedSearch,
       record: this.props.record
    }
     this.componentWillMount = props.componentWillMount;
       this.getSearchProduct = props.getSearchProduct;
       this.saveSearchValue = props.saveSearchValue;
       this.deleteSavedSearch = props.deleteSavedSearch;
       this.runSearch = props.runSearch;
       this.ShowAsserts =  this.ShowAsserts.bind(this);
       this.sendToQuad = props.sendToQuad.bind(this);
}

/** @function componentWillReceiveProps -
 * It sets the next props value to the existing component
*/
componentWillReceiveProps(nextProps) {
  this.setState({isSavedSearch: nextProps.isSavedSearch});
  this.setState({record: nextProps.record});
}
/** @function ShowAsserts -
 * This function is used to show and hide left content(Difficulty levels)
*/
ShowAsserts(e){
  this.setState({showAssert: !this.state.showAssert})
}
/**
 * @function render -
 * When called, it will render the search library component
 * @return {string}
 * HTML markup of the component
*/
   render() {
    const {
        fields: {productName}, handleSubmit
      } = this.props;
      const sliderRes = this.state.showAssert? {width: '16%'}: {width: '1%'};
      const assetsRes = this.state.showAssert? {width: '83%'}: {width: '99%'};
      const saveSearchDiv = {float: 'right',marginTop:5};
      const displayContainerDiv = {display:'none'};
      let SearchValue = 'The Humanities: Culture, Continuity and Change, Volume II';
      if(this.props.SearchValue!==undefined){
        SearchValue = this.props.SearchValue;
      }

return (
   <div>
      <form>
        
            <div className="searchLib">
              <span className="searchTitle">Searching in :</span> 
              <span className="searchVal"> {SearchValue}</span>
            </div>
             
             <div className='pe-input-group input-grand-container'>
                <div className='pe-input input-container'>
                   <SearchCompleteContainer id='productName' value={productName} />
                    <button id='searchButton'
                    onClick={handleSubmit(this.getSearchProduct)}
                    className='pe-btn pe-btn--medium'>
                    <i className="fa fa-search" aria-hidden="true"></i>
                    </button>
                </div>       
              </div>
      </form>
      <div className='row' id='displayContainerDiv' style={displayContainerDiv}>
        <div className='pe-slider' style={sliderRes}>
            {!this.state.showAssert ?
            <button onClick={handleSubmit(this.ShowAsserts)} className='pe-btn pe-btn--medium'>
                <i className='fa fa-angle-right' aria-hidden='true'></i>
            </button> : ''} {this.state.showAssert ?
            <div className='slider'>
                <button onClick={handleSubmit(this.ShowAsserts)} className='pe-btn pe-btn--medium'>
                    <i className='fa fa-angle-left' aria-hidden='true'></i>
                </button>
                <SearchFilterContainer /> { /*<a href='#' onClick={handleSubmit(this.saveSearchValue)}>Save Search</a>*/ }
            </div> : ''}
        </div>
        <div className='col-md-10' style={assetsRes}>
            <SearchAssetFiltersContainer />
        </div>
        <div className={this.state.isSavedSearch ? 'pe-btn-bar hidden searchBar' : 'pe-btn-bar searchBar'}>
            <button className='pe-btn'>Cancel</button>
            <button className='pe-btn pe-btn--primary selectBtn' onClick={this.sendToQuad}>Select</button>
        </div>
    </div>
  </div>
    )

}
}

SearchLibrary.propTypes = {
  isSavedSearch: PropTypes.bool,
  record:PropTypes.object,
  componentWillMount: PropTypes.func,
  getSearchProduct:PropTypes.func,
  saveSearchValue: PropTypes.func,
  deleteSavedSearch: PropTypes.func,
  runSearch: PropTypes.func,
  ShowAsserts: PropTypes.func,
  sendToQuad: PropTypes.func,
  handleSubmit: PropTypes.func,
  fields: PropTypes.object,
  SearchValue: PropTypes.string
}

SearchLibrary = reduxForm({
  form: 'SearchLibrary',
  fields: ['productName']
})(SearchLibrary);

SearchLibrary.PropTypes = {

}

module.exports= SearchLibrary;
