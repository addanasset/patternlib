/**
 * Copyright (c) Pearson, Inc.
 * All rights reserved.
 *
 * It is a Items display per page component.
 * This component operates as a "Controller-View".
 *
 * @module MediaAssets
 * @file BrowseAssets
 * @author TDC
 *
*/
import React, { Component, PropTypes } from 'react';
import AddAnAsset from './AddAnAsset';
import AssetFiltersContainer from '../container/AssetFiltersContainer';
import FolderTree from '../container/TreePaneContainer';
import { Scrollbars } from 'react-custom-scrollbars';

class BrowseAssets extends Component {
    /**
  * @constructor defines states of the Browse Assets Component
  */
    constructor(props) {
        super(props);
        this.state = {
            record: this.props.record,
        };
        this.sendToQuad = props.sendToQuad.bind(this);
    }
    /** @function componentWillReceiveProps -
 * It sets the next props value to the component
*/
    componentWillReceiveProps(nextProps) {
        this.state.record = nextProps.record;
    }
/**
 * @function render -
 * When called, it will render the Browse Asset component
 * @return {string}
 * HTML markup of the component
*/
    render() {
      return (
        <div className="search-tab browse-library">
            <div>
              <span className="browseTitle">Browsing in : </span>
              <span className="browseResult">The Humanities: Culture, Continuity and Change, Volume II</span>
            </div>
            <div className="row main">
                
                <div className="filter-container">
                    
                        <div className="filters-label">Documents</div>
                        <div className="filters-content">
                        <Scrollbars style={{ height: 350 }}>
                        <FolderTree/>
                        </Scrollbars>
                        </div>
                    
                </div>
               
               
                <div className="pe-search-results-container browse-tab-results">
                    <AssetFiltersContainer />
                </div>
                <div className="pe-btn-bar">
                    <button className="pe-btn pe-cancel-btn" type="button">Cancel</button>
                    <button className="pe-btn pe-btn--primary selectBtn" onClick={this.sendToQuad} type="button">Select</button>
                </div>
            </div>
        </div>
      )
    }
}

BrowseAssets.propTypes = {
    record: PropTypes.object,
    sendToQuad: PropTypes.func
}

module.exports = BrowseAssets;
