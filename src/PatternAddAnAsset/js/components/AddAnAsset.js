/**
 * Copyright (c) Pearson, Inc.
 * All rights reserved.
 *
 * It is a Items display per page component.
 * This component operates as a "Controller-View".
 *
 * @module MediaAssets
 * @file AddAnAsset
 * @author TDC
 *
*/
import React, { Component, PropTypes } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import FileUploadContainer from '../container/FileUploadContainer';
import CheckJobStatusContainer from '../container/CheckJobStatusContainer';
import UploadProgressContainer from '../container/UploadProgressContainer';
import SearchLibrary from './SearchLibrary';
import BrowseAssetsContainer from '../container/BrowseAssetsContainer';
import SearchLibraryContainer from '../container/SearchLibraryContainer';
import ReviewAssetContainer from 
       '../../../PatternReviewAsset/js/containers/ReviewAssetMetadataContainer'
import { Link, browserHistory, hashHistory } from 'react-router';
class AddAnAsset extends Component {
   /**
  * @constructor defines states of the Add an asset Component
  */
  constructor(props) {
    super(props);
    this.onSave = props.onSave;
  }

   /** @function handleSelect -
 * This function is called on tab selection
  */
  handleSelect(index, last) {
    sessionStorage.currentTab= index;
    hashHistory.push('/');
  }
  /**
 * @function render -
 * When called, it will render the Add an asset component with its tabs
 * @return {string}
 * HTML markup of the component
*/
  render() {
  let isError = true;
  let indexVal;
  let viewController;
  if(!sessionStorage.currentTab){
    indexVal=0;
    sessionStorage.currentTab=indexVal;
  }else{
    indexVal = parseInt(sessionStorage.currentTab);
  }

  let fileuploadContainer = <FileUploadContainer />;
  let checkJobStatus = <CheckJobStatusContainer />;
  let uploadProgress = <UploadProgressContainer />;
  let initviewController = <ReviewAssetContainer/>;
  if(this.props.children != null){
    fileuploadContainer = '';
	   	if(this.props.children.props.location.pathname === '/UploadInProgress'){
	   	checkJobStatus = '';
	    }
	    if(this.props.children.props.location.pathname === '/CheckJobStatus'){
	   	  uploadProgress = '';
	    }

	    if(this.props.children.props.location.pathname === '/errorUploading'){
          checkJobStatus = '';
          uploadProgress = <UploadProgressContainer error={isError} />;
        }
        if(this.props.children.props.location.pathname === '/ReviewAsset'){
          viewController = <ReviewAssetContainer  ref="ReviewAssetsContainer" patConfig={this.props.children.props.route.patConfig} libConfig={this.props.children.props.route.libConfig} />;
        }


  }else{
  	checkJobStatus = '';
  	uploadProgress = '';
  } 
    if(viewController === undefined){

       viewController = (<Tabs id="addAnAssets" onSelect={this.handleSelect} selectedIndex={indexVal}>
          <TabList className="parentTab">
            <Tab>Search Library</Tab>
            <Tab>Browse Assets</Tab>
            <Tab>Upload Files</Tab>
          </TabList>
          <TabPanel>
           <SearchLibraryContainer />
          </TabPanel>
          <TabPanel className="browseAssets">
           <BrowseAssetsContainer />
          </TabPanel>
        <TabPanel className="uploadAssets">
          <div>
          {fileuploadContainer}
          </div>
          <div>
          {checkJobStatus}
          </div>
          <div>
          {uploadProgress}
          </div>
       </TabPanel>
      </Tabs>)
    }
    return (
      <div className="pe-uploadasset">
      {viewController}
      </div>
    )


  }
}

AddAnAsset.propTypes = {
  children: PropTypes.object,
  onSave: PropTypes.func
}

module.exports = AddAnAsset;
