import React, { Component, PropTypes } from 'react';

let interval = {};

class ProgressBarComp extends Component{

        constructor() {
            super();
            this.state = {
                start: true,
                stop: false
            };
        }
        componentWillReceiveProps(nextProps) {
            this.setState({percentage: nextProps.percentage + '%'});
        }
        shouldComponentUpdate(nextProps, nextState) {
            if (nextState.stop === true) {
                clearInterval(interval);
            }
            return (nextState.stop !== true);
        }

        stopProgress() {
            this.setState({stop: true});
        }
        render() {
            return (
                    < div id = 'show-progress-with-jsx' ref = 'foo' >
                    < div className = 'progress' >
                    < div className = 'progress-bar'
                    style = {{width:this.props.percentage+'%'}} > < /div>
                < /div>
                < /div>
                );
        }
}

ProgressBarComp.propTypes = {
    percentage: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number,
      
    ])
}

module.exports = ProgressBarComp;
