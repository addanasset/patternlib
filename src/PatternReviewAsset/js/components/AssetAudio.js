import React from 'react'

const flowplayerAudio =  { width: 600 };

const AssetAudio = (props) => { 
return(<div className="pe-input pe-input--horizontal">
<div id="reviewAssestAudio" className="playful" style={flowplayerAudio}>
	<audio controls="controls"><source type="audio/mpeg" src={props.url} /></audio>
</div>
</div>)
}

AssetAudio.propTypes = {
	url:React.PropTypes.string
}

export default AssetAudio;
