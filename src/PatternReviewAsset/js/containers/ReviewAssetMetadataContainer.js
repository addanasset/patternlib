import { connect } from 'react-redux';
import { fetchAssetMetaData, populateReviewForm, saveReviewForm } from '../actions';
import ReviewAssetMetadata from '../components/ReviewAssetMetadata'; 
import MetadataUtils from '../util/MetadataUtils';
import {getCurrentValues} from '../../../PatternAddAnAsset/js/utils/util';

const getSelectedValues = (dataArray) => {
  if (dataArray !== undefined && dataArray.length > 0) {
    return dataArray[dataArray.length-1];
  }

  return [];
}

const mapStateToProps = (state,ownProps) => {

  const data = getSelectedValues(state.ReviewAssetReducers); 
  let quadData = getCurrentValues(state.quad)
  return {    
    errMsg: data.errMsg,
    mimetype:data.mimetype,
    url:data.url,
    uuid:data.uuid,
    ContentType:data.ContentType,
    name:data.name,
    filename:data.filename,
    altText:data.altText,
    Caption:data.Caption,
    copyRightInfo:data.copyRightInfo,
    Transcript:data.Transcript,
    ClosedCaption:data.ClosedCaption,
    pafId:ownProps.patConfig.patSetup.pafId,
    difficultyLevel:data.difficultyLevel,
    alignmentObjective:data.alignmentObjective,
    nodeRef:data.nodeRef,
    mimetype:data.format,
    path:data.path,
    assetData : quadData,
    thumbnail:data.thumbnailUrl,
    diffLevel:data.diffLevel,
    contextSpecificAltText:'',
 	nameAltText:'',
 	ticket:data.ticket,
    'initialValues': {    								    				    				    				    				    
				    alignmentObjective: '',	
				    nameAltText:'',			
				    contextSpecificAltText:'',				    
				    filename:data.filename,
				    ContentType: (data.format == '' ||  data.format == undefined)?'':
                         _.upperFirst(data.format.split('/')[0]),
				    pafId:ownProps.patConfig.patSetup.pafId,
				    difficultyLevel:'',
				  }
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
	let resultObj = {		
			'uuid' : 'd6f77c93-704a-4109-8391-d6e13e290f96',
	}

	let metadata = {};
	
	if(ownProps && ownProps.patConfig){
		metadata.patConfig  = ownProps.patConfig;
		if(ownProps.patConfig.patSetup){ // getting UUID from external input - QUAD
			metadata.uuid = ownProps.patConfig.patSetup.uuid;
			metadata.caption = ownProps.patConfig.patSetup.caption;
			metadata.altText = ownProps.patConfig.patSetup.altText;
			metadata.copyrtInfo = ownProps.patConfig.patSetup.copyrtInfo;

		}
	}

	 if(ownProps && ownProps.libConfig){
      metadata.libConfig = ownProps.libConfig;
     }
    if(metadata.uuid === '')
    	metadata.uuid = this.props.assetData.uuid;
    
    
    if(metadata.uuid == undefined){
       metadata.nodeRefManiFest = dispatch((() => { return (dispatch,getState) => {
     		   return  _.chain(getState().quad.last().nodeRef).split('/').last().value();							
	 			} 
	 		}
 		)());       
    }

    
   
   return {

    componentWillMount() { 
		dispatch(fetchAssetMetaData(metadata));
    },

    onSave(values, dispatch, ownProps){ 
     let resultMLData  = values;          
     let resultLocalC3 = {};

     if(values.ContentType == 'Audio' ||  values.ContentType == 'Video'){
       resultMLData.Transcript = CKEDITOR.instances['TranscriptCk'].getData();
       resultMLData.ClosedCaption = CKEDITOR.instances['ClosedCaptionCk'].getData();     	
     }
     resultLocalC3.altTextCk = CKEDITOR.instances['altTextCk'].getData();
     resultLocalC3.CaptionCk = CKEDITOR.instances['CaptionCk'].getData();
     resultLocalC3.copyRightCk = CKEDITOR.instances['copyRightCk'].getData();
     resultMLData.name = CKEDITOR.instances['nameCk'].getData();
     dispatch(saveReviewForm(values,resultMLData,ownProps.patConfig,ownProps.libConfig));
    }
  }
}

const ReviewAssetMetaContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ReviewAssetMetadata);

export default ReviewAssetMetaContainer;
